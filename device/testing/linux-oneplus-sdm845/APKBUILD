# Maintainer: Caleb Connolly <caleb@connolly.tech>
# Kernel config based on: arch/arm64/configs/defconfig

_flavor="oneplus-sdm845"
pkgname=linux-$_flavor
pkgver=5.8_rc6
pkgrel=0
pkgdesc="Kernel fork for OnePlus SDM845 devices, close to mainline"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/sdm845-mainline/sdm845-linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="bison findutils flex installkernel openssl-dev perl"

_config="config-$_flavor.$arch"
_commit="57dc9bcca97d7f8a5e79de5b45ca7cd5d66ceef5"

# Source
source="
	sdm845-linux-$_commit.tar.gz::https://gitlab.com/sdm845-mainline/sdm845-linux/-/archive/$_commit/sdm845-linux-$_commit-sdm845.tar.gz
	$_config
"
builddir="$srcdir/sdm845-linux-$_commit"

prepare() {
	default_prepare
	mkdir -p "$srcdir"/build
	cp -v "$srcdir"/$_config "$srcdir"/build/.config
	make -C "$builddir" O="$srcdir"/build ARCH="$_carch" \
		olddefconfig
}

build() {
	cd "$srcdir"/build
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	cd "$srcdir/build/arch/$_carch/boot"

	install -D "$srcdir/build/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	cd "$srcdir"/build
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot/ \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"
}

sha512sums="c05ea0b5191e9961613e16c8693ab1afffbbedced2a3223999f065cfe18225c53dff25e1c4a7ff0c7d883d80dc431ae8e2b46d812afc31abd63c25d9c9adb1b6  sdm845-linux-57dc9bcca97d7f8a5e79de5b45ca7cd5d66ceef5.tar.gz
0f0760669f931c7b40cb12a3ded1597b4c27642ee89ee06ed2a65db12c6492ae24fb920d376db3cfa56f114c1dc842dd08c7df622a5076b83961e3b4ea3c3f9a  config-oneplus-sdm845.aarch64"
