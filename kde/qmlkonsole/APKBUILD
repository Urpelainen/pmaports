# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qmlkonsole
pkgver=0_git20200827
pkgrel=0
_commit="61017be0ffd4d35b4c462625a6aba365bbacb4b4"
pkgdesc="Terminal app for Plasma Mobile"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
url="https://invent.kde.org/jbbgameich/qmlkonsole"
license="GPL-3.0-or-later"
depends="qmltermwidget kirigami2"
makedepends="extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtsvg-dev qt5-qtquickcontrols2-dev kirigami2-dev ki18n-dev"
source="https://invent.kde.org/plasma-mobile/qmlkonsole/-/archive/$_commit/qmlkonsole-$_commit.tar.gz"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

prepare() {
	default_prepare

	# qmlplugindump fails for armv7+qemu (pmb#1970). This is purely for
	# packager knowledge and doesn't affect runtime, so we can disable it.
	if [ "$CARCH" = "armv7" ]; then
		sed -i "s/ecm_find_qmlmodule/# ecm_find_qmlmodule/g" CMakeLists.txt
	fi
}

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="33596da9dd2e2480b68d007a9297a19938e558d0ef2a6a0b704c61291c88a48341b3c680aa0727c6235e7452c0e5d8dd61603b95756033c5a1ac21cc0c93b038  qmlkonsole-61017be0ffd4d35b4c462625a6aba365bbacb4b4.tar.gz"
